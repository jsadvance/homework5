'use strict'

// ## Завдання

// - Створити сторінку, яка імітує стрічку новин соціальної мережі [Twitter](https://twitter.com/).

// #### Технічні вимоги:

//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`. При необхідності ви можете додавати також інші класи.

// #### Необов'язкове завдання підвищеної складності

//  - Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
//  - Додати зверху сторінки кнопку `Додати публікацію`. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  `https://ajax.test-danit.com/api/json/posts`. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з `id: 1`.
//  - Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.

const root = document.querySelector('#root')

const USER = 'https://ajax.test-danit.com/api/json/users'
const POST = 'https://ajax.test-danit.com/api/json/posts'

class Request {
  request(url) {
    return fetch(url).then((response) => {
      return response.json()
    })
  }

  getInfoServer(url) {
    return this.request(url)
  }
}

class Card {
  render(array) {
    const [user, post] = array

    let fragment = document.createDocumentFragment()

    user.map(({ id: idUser, name, username, email }) => {
      post.filter(({ userId, title: titleId, body }) => {
        if (userId === idUser) {
          let deletePost = document.createElement('img')
          deletePost.src = './delete.png'
          deletePost.classList.add('deletePost')
          let conteiner = document.createElement('div')
          conteiner.classList.add('conteiner')
          let userFullName = document.createElement('p')
          userFullName.classList.add('user')
          let userEmail = document.createElement('p')
          let titlePost = document.createElement('p')
          titlePost.classList.add('titlePost')
          let textPost = document.createElement('p')

          userFullName.innerHTML = ` ${name} ${username} <img class="img" src="./ok.png" alt=""> <span class="email">${email}</span>`

          titlePost.innerHTML = `<img class="img" src="./flag.png" alt=""> ${titleId}`
          textPost.textContent = body

          deletePost.addEventListener('click', () => {
            fetch(`${POST}/${userId}`, {
              method: 'DELETE',
            }).then((response) => {
              if (response.ok) {
                conteiner.remove()
              }
            })
          })

          conteiner.append(
            userFullName,
            userEmail,
            titlePost,
            textPost,
            deletePost
          )
          fragment.append(conteiner)
        }
      })
    })

    return fragment
  }
}

let card = new Card()
let request = new Request()

let getUser = request.getInfoServer(USER).then((data) => data)
let getPost = request.getInfoServer(POST).then((data) => data)

Promise.all([getUser, getPost]).then((data) => {
  root.append(card.render(data))
})
